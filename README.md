# SSH Tools for vscode

## 简介

- 支持远程连接linux和windows系统(图形化界面、命令行、sftp)
- 支持添加、修改和删除主机连接信息
- 支持导入、导出和清空配置信息
- 支持将远程目录添加到工作区，精简视图，方便操作
- SSH Tools活动栏支持在线、离线和管理三种视图
- Workspace for SSH Tools 活动栏支持在线、离线两种视图
- 支持分组管理
- 详细信息请查看 “更改日志”

## 连接Windows主机(OpenSSH For Windows)

- 连接Windows主机时，需要安装 OpenSSH For Windows,且开启对应端口(默认：22)
- [下载](https://github.com/PowerShell/Win32-OpenSSH/releases)   [安装文档](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse)

## 使用linux和windows 的图形化远程桌面

- 支持linux和windows 的图形化远程桌面,注意需要在主机配置页面开启"启用远程"
  
- Windows: use mstsc
  
- Linux: use xrdp(server) and freerdp(client),Need to install xrdp and freerdp packages 
  
- Default Port: 3389

## 其他

[提交Bug](https://gitee.com/xplot/sshtools/issues)

## 重置配置文件

- 某些因为所导入配置文件异常，会引起读取系统配置信息异常，影响正常使用

- 点击 清空配置 就会重置配置文件，所有配置信息将会被清空！！！

## 截图

[![R6qcVg.gif](https://z3.ax1x.com/2021/07/02/R6qcVg.gif)](https://imgtu.com/i/R6qcVg)